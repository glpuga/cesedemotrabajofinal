/* Copyright 2016, Gerardo Puga
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief ciaa_leon3_demo source file
 **
 **
 **/

/** \addtogroup CIAA_Firmware CIAA Firmware
 ** @{ */
/** \addtogroup Examples CIAA Firmware Examples
 ** @{ */
/** \addtogroup ciaa_leon3_demo ciaa_leon3_demo example source file
 ** @{ */

/*
 * Initials     Name
 * ---------------------------
 * GLP          Gerardo Puga
 * */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20161203 v0.0.1   PR   first functional version
 */

/*==================[inclusions]=============================================*/

#include "grlib.h"
#include "screen.h"

/*==================[macros and definitions]=================================*/


/*==================[internal data declaration]==============================*/


/*==================[internal functions declaration]=========================*/


/*==================[internal data definition]===============================*/


uint32_t screenControllerAddress;

uint8_t screenBufferPing[TEXT_SCREEN_BUFFER_SIZE];
uint8_t screenBufferPong[TEXT_SCREEN_BUFFER_SIZE];

uint8_t *activeBufferPtr;
uint8_t *shadowBufferPtr;


/*==================[external data definition]===============================*/


/*==================[internal functions definition]==========================*/


void screenInternalWriteCharacter(int32_t col, int32_t row, int8_t dataByte)
{
   uint32_t linearAddress;

   linearAddress = row * TEXT_SCREEN_WIDTH + col;

   grRegisterWrite(screenControllerAddress, GRLIB_APBVGA_DATA_REGISTER,
         GRLIB_SHIFT_FIELD_APBVGA_DATA_REGISTER_ADDRESS(linearAddress) | GRLIB_SHIFT_FIELD_APBVGA_DATA_REGISTER_DATA(dataByte));
}


void screenInternalSetBackgroundColor(int32_t red, int32_t green, int8_t blue)
{
   grRegisterWrite(screenControllerAddress, GRLIB_APBVGA_BACKGROUND_COLOR_REGISTER,
         GRLIB_SHIFT_FIELD_APBVGA_BACKGROUND_COLOR_REGISTER_RED(red)
         | GRLIB_SHIFT_FIELD_APBVGA_BACKGROUND_COLOR_REGISTER_GREEN(green)
         | GRLIB_SHIFT_FIELD_APBVGA_BACKGROUND_COLOR_REGISTER_BLUE(blue));
}


void screenInternalSetForegroundColor(int32_t red, int32_t green, int8_t blue)
{
   grRegisterWrite(screenControllerAddress, GRLIB_APBVGA_FOREGROUND_COLOR_REGISTER,
         GRLIB_SHIFT_FIELD_APBVGA_FOREGROUND_COLOR_REGISTER_RED(red)
         | GRLIB_SHIFT_FIELD_APBVGA_FOREGROUND_COLOR_REGISTER_GREEN(green)
         | GRLIB_SHIFT_FIELD_APBVGA_FOREGROUND_COLOR_REGISTER_BLUE(blue));
}


void screenInternalBlankBuffer(uint8 *bufferPtr)
{
   int32_t i;

   for (i = 0; i < TEXT_SCREEN_BUFFER_SIZE; i++)
   {
      bufferPtr[i] = (uint8_t)' ';
   }
}


void screenInternalFullScreenRefresh()
{
   int32_t col, row;

   for (row = 0; row < TEXT_SCREEN_HEIGHT; row++)
   {
      for (col = 0; col < TEXT_SCREEN_WIDTH; col++)
      {
         screenInternalWriteCharacter(col, row, activeBufferPtr[row * TEXT_SCREEN_WIDTH + col]);
      }
   }
}


void screenInternalUpdateScreen()
{
   int32_t col, row;
   int32_t bufferIndex;

   for (row = 0; row < TEXT_SCREEN_HEIGHT; row++)
   {
      for (col = 0; col < TEXT_SCREEN_WIDTH; col++)
      {
         bufferIndex = row * TEXT_SCREEN_WIDTH + col;

         if (activeBufferPtr[bufferIndex] != shadowBufferPtr[bufferIndex])
         {
            screenInternalWriteCharacter(col, row, activeBufferPtr[bufferIndex]);
         }
      }
   }
}


void screenInternalSwapBufferRoles()
{
   if (activeBufferPtr == screenBufferPing)
   {
      activeBufferPtr = screenBufferPong;
      shadowBufferPtr = screenBufferPing;
   } else {
      activeBufferPtr = screenBufferPing;
      shadowBufferPtr = screenBufferPong;
   }
}


/*==================[external functions definition]==========================*/


void screenInitModule()
{
   static grPlugAndPlayAPBDeviceTableEntryType apbDeviceInfo; /* static so that the stack is not used for storage */
   int32_t retCode;

   activeBufferPtr = screenBufferPing;
   shadowBufferPtr = screenBufferPong;

   screenInternalBlankBuffer(activeBufferPtr);
   screenInternalBlankBuffer(shadowBufferPtr);

   /* Detect the hardware address of the APBVGA core */
   retCode = grWalkPlugAndPlayAPBDeviceTable(
         GRLIB_PNP_VENDOR_ID_GAISLER_RESEARCH,
         GRLIB_PNP_DEVICE_ID_APBVGA,
         &apbDeviceInfo,
         0);

   if (retCode < 0)
   {
      ShutdownOS(E_OK);
   }

   screenControllerAddress = apbDeviceInfo.address;

   screenInternalSetBackgroundColor(0x00, 0x00, 0x00);
   screenInternalSetForegroundColor(0xff, 0xff, 0xff);

   screenInternalFullScreenRefresh();
}


void screenSetBackgroundColor(int32_t red, int32_t green, int8_t blue)
{
   screenInternalSetBackgroundColor(red, green, blue);
}


void screenSetForegroundColor(int32_t red, int32_t green, int8_t blue)
{
   screenInternalSetForegroundColor(red, green, blue);
}


uint8_t *screenGetActiveScreenBufferPtr()
{
   return activeBufferPtr;
}


void screenUpdateScreen()
{
   screenInternalUpdateScreen();

   screenInternalSwapBufferRoles();

   screenInternalBlankBuffer(activeBufferPtr);
}


/* **************************** */

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/

