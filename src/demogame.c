/* Copyright 2016, Gerardo Puga
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief ciaa_leon3_demo source file
 **
 **
 **/

/** \addtogroup CIAA_Firmware CIAA Firmware
 ** @{ */
/** \addtogroup Examples CIAA Firmware Examples
 ** @{ */
/** \addtogroup ciaa_leon3_demo ciaa_leon3_demo example source file
 ** @{ */

/*
 * Initials     Name
 * ---------------------------
 * GLP          Gerardo Puga
 * */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20161203 v0.0.1   PR   first functional version
 */

/*==================[inclusions]=============================================*/


#include "demogame.h"
#include "screen.h"
#include "sprite.h"
#include <string.h>
#include <stdio.h>


/*==================[macros and definitions]=================================*/


#define DEMOGAME_BG_RED    0x30
#define DEMOGAME_BG_GREEN  0x30
#define DEMOGAME_BG_BLUE   0x30


#define DEMOGAME_FG_RED    0xff
#define DEMOGAME_FG_GREEN  0xff
#define DEMOGAME_FG_BLUE   0xff


#define DEMOGAME_FIELD_WIDTH   (TEXT_SCREEN_WIDTH)
#define DEMOGAME_FIELD_HEIGHT  (TEXT_SCREEN_HEIGHT)

#define DEMOGAME_FIELD_SIZE    (DEMOGAME_FIELD_WIDTH * DEMOGAME_FIELD_HEIGHT)

#define DEMOGAME_BALL_SPEED_DIVIDER 3

#define DEMOGAME_JUMP_DIVIDER       150

#define DEMOGAME_EMPTY_SPOT_CHAR ' '
#define DEMOGAME_BALL_CHAR       'O'


#define DEMOGAME_SHIPS_COUNT 2

#define DEMOGAME_TEXT_BOX_WIDTH   24
#define DEMOGAME_TEXT_BOX_HEIGHT  5

typedef struct {

   int32_t xPos;
   int32_t yPos;

   int32_t jumpDivider;
   int32_t currentJumpDelta;

} demoGameShipStateType;


typedef struct {

   int32_t xPos;
   int32_t yPos;

   int32_t xVel;
   int32_t yVel;

   int32_t speedDivider;

} demoGameBallStateType;


typedef struct {

   int32_t xUpperLimit;
   int32_t xLowerLimit;

} demoGameShipRestrictionType;


/*==================[internal data declaration]==============================*/


demoGameShipStateType demoGameShipsState[DEMOGAME_SHIPS_COUNT];

demoGameShipRestrictionType demoGameShipsRestrictions[DEMOGAME_SHIPS_COUNT];

demoGameBallStateType demoGameBallState;

uint8_t demoGameCanvas[DEMOGAME_FIELD_SIZE];


char demoGameStringsTable[DEMOGAME_TEXT_BOX_HEIGHT][DEMOGAME_TEXT_BOX_WIDTH + 1];

int32_t demoGameStringsHead;

int32_t demoGameStringsRow;

int32_t demoGameStringsCol;


int32_t demoGameUptimeHours;

int32_t demoGameUptimeMinutes;

int32_t demoGameUptimeSeconds;


/*==================[internal functions declaration]=========================*/


void demoGameInternalDrawHorLine(int32_t x1, int32_t x2, int32_t y, uint8_t *canvasPtr, char dataByte)
{
   int32_t bufferIndex;
   int32_t xIndex;

   if (x2 < x1)
   {
      xIndex = x2;
      x2     = x1;
      x1     = xIndex;
   }

   if (x1 >= DEMOGAME_FIELD_WIDTH)
   {
      return;
   }

   if (x2 >= DEMOGAME_FIELD_WIDTH)
   {
      x2 = DEMOGAME_FIELD_WIDTH - 1;
   }

   if (y >= DEMOGAME_FIELD_HEIGHT)
   {
      return;
   }

   bufferIndex = y * DEMOGAME_FIELD_WIDTH + x1;

   for (xIndex = x1; xIndex <= x2; xIndex++)
   {
      canvasPtr[bufferIndex] = dataByte;

      bufferIndex++;
   }
}


void demoGameInternalDrawVerLine(int32_t x, int32_t y1, int32_t y2, uint8_t *canvasPtr, char dataByte)
{
   int32_t bufferIndex;
   int32_t yIndex;

   if (y2 < y1)
   {
      yIndex = y2;
      y2     = y1;
      y1     = yIndex;
   }

   if (x >= DEMOGAME_FIELD_WIDTH)
   {
      return;
   }

   if (y1 >= DEMOGAME_FIELD_HEIGHT)
   {
      return;
   }

   if (y2 >= DEMOGAME_FIELD_HEIGHT)
   {
      y2 = DEMOGAME_FIELD_HEIGHT - 1;
   }

   bufferIndex = y1 * DEMOGAME_FIELD_WIDTH + x;

   for (yIndex = y1; yIndex <= y2; yIndex++)
   {
      canvasPtr[bufferIndex] = dataByte;

      bufferIndex += DEMOGAME_FIELD_WIDTH;
   }
}


void demoGameInternalDrawChar(int32_t x, int32_t y, uint8_t *canvasPtr, char dataByte)
{
   int32_t bufferIndex;

   if ((x < DEMOGAME_FIELD_WIDTH) && (y < DEMOGAME_FIELD_HEIGHT))
   {
      bufferIndex = y * DEMOGAME_FIELD_WIDTH + x;
      canvasPtr[bufferIndex] = dataByte;
   }
}


void demoGameInternalPrintString(int32_t x, int32_t y, uint8_t *canvasPtr, const char *string)
{
   int32_t bufferIndex;
   int32_t stringLength;
   int32_t stringIndex;

   stringLength = strlen(string);

   if (x >= DEMOGAME_FIELD_WIDTH)
   {
      x = DEMOGAME_FIELD_WIDTH - 1;
   }

   if (x + stringLength >= DEMOGAME_FIELD_WIDTH)
   {
      stringLength = DEMOGAME_FIELD_WIDTH - x;
   }

   bufferIndex = y * DEMOGAME_FIELD_WIDTH + x;

   for (stringIndex = 0; stringIndex < stringLength; stringIndex++)
   {
      canvasPtr[bufferIndex] = string[stringIndex];
      bufferIndex++;
   }
}


void demoGameInternalDrawSprite(int32_t x, int32_t y, uint8_t *canvasPtr, const spriteDataType *spriteData)
{
   int32_t leftWidth, rightWidth;
   int32_t upperHeight, lowerHeight;
   int32_t canvasIndex, spriteIndex;

   int32_t currentX, currentY;

   leftWidth  = spriteData->width / 2;
   upperHeight = spriteData->height / 2;

   rightWidth  = spriteData->width - leftWidth;
   lowerHeight = spriteData->height - upperHeight;


   if (((x - leftWidth) < 0) || ((x + rightWidth) >= DEMOGAME_FIELD_WIDTH))
   {
      return;
   }

   if (((y - upperHeight) < 0) || ((y + lowerHeight) >= DEMOGAME_FIELD_HEIGHT))
   {
      return;
   }

   canvasIndex = (y - upperHeight) * DEMOGAME_FIELD_WIDTH + (x - leftWidth);
   spriteIndex = 0;

   for (currentY = 0; currentY < spriteData->height; currentY++)
   {
      for (currentX = 0; currentX < spriteData->width; currentX++)
      {
         canvasPtr[canvasIndex] = spriteData->body[spriteIndex];
         canvasIndex++;
         spriteIndex++;
      }

      canvasIndex += DEMOGAME_FIELD_WIDTH - spriteData->width;
   }
}


void demoGameInternalInitFieldGame()
{
   int32_t canvasIndex;

   for (canvasIndex = 0; canvasIndex < DEMOGAME_FIELD_SIZE; canvasIndex++)
   {
      demoGameCanvas[canvasIndex] = DEMOGAME_EMPTY_SPOT_CHAR;
   }

   /*
    * Field box
    */
   demoGameInternalDrawVerLine(0, 0, DEMOGAME_FIELD_HEIGHT - 1, demoGameCanvas, '[');
   demoGameInternalDrawVerLine(1, 0, DEMOGAME_FIELD_HEIGHT - 1, demoGameCanvas, ']');

   demoGameInternalDrawVerLine(DEMOGAME_FIELD_WIDTH - 1, 0, DEMOGAME_FIELD_HEIGHT - 1, demoGameCanvas, ']');
   demoGameInternalDrawVerLine(DEMOGAME_FIELD_WIDTH - 2, 0, DEMOGAME_FIELD_HEIGHT - 1, demoGameCanvas, '[');

   demoGameInternalDrawHorLine(1, DEMOGAME_FIELD_WIDTH - 2, 0, demoGameCanvas, '-');
   demoGameInternalDrawHorLine(1, DEMOGAME_FIELD_WIDTH - 2, DEMOGAME_FIELD_HEIGHT - 1, demoGameCanvas, '-');

   /*
    * Text box
    * */

   demoGameInternalDrawVerLine(
         DEMOGAME_FIELD_WIDTH - DEMOGAME_TEXT_BOX_WIDTH - 4,
         DEMOGAME_FIELD_HEIGHT - DEMOGAME_TEXT_BOX_HEIGHT - 4,
         DEMOGAME_FIELD_HEIGHT - 2,
         demoGameCanvas, '[');

   demoGameInternalDrawVerLine(
         DEMOGAME_FIELD_WIDTH - DEMOGAME_TEXT_BOX_WIDTH - 3,
         DEMOGAME_FIELD_HEIGHT - DEMOGAME_TEXT_BOX_HEIGHT - 2,
         DEMOGAME_FIELD_HEIGHT - 2,
         demoGameCanvas, '>');

   demoGameInternalDrawHorLine(
         DEMOGAME_FIELD_WIDTH - DEMOGAME_TEXT_BOX_WIDTH - 3,
         DEMOGAME_FIELD_WIDTH - 2,
         DEMOGAME_FIELD_HEIGHT - DEMOGAME_TEXT_BOX_HEIGHT - 4,
         demoGameCanvas, '-');

   demoGameInternalDrawHorLine(
         DEMOGAME_FIELD_WIDTH - DEMOGAME_TEXT_BOX_WIDTH - 3,
         DEMOGAME_FIELD_WIDTH - 2,
         DEMOGAME_FIELD_HEIGHT - DEMOGAME_TEXT_BOX_HEIGHT - 2,
         demoGameCanvas, '-');

   /*
    * Central banner
    * */
   demoGameInternalDrawSprite(DEMOGAME_FIELD_WIDTH / 2, DEMOGAME_FIELD_HEIGHT * 2 / 5, demoGameCanvas, &spriteBanner);
}


void demoGameInternalClearMobiles()
{
   /* Ball */
   demoGameInternalDrawChar(demoGameBallState.xPos, demoGameBallState.yPos, demoGameCanvas, DEMOGAME_EMPTY_SPOT_CHAR);

   /* Lower ship */
   demoGameInternalDrawHorLine(2, DEMOGAME_FIELD_WIDTH - DEMOGAME_TEXT_BOX_WIDTH - 5, demoGameShipsState[0].yPos, demoGameCanvas, DEMOGAME_EMPTY_SPOT_CHAR);

   /* Upper ship */
   demoGameInternalDrawHorLine(2, DEMOGAME_FIELD_WIDTH - 3, demoGameShipsState[1].yPos, demoGameCanvas, DEMOGAME_EMPTY_SPOT_CHAR);
}


void demoGameInternalCheckColissions()
{
   int32_t actualObstacles[9];
   int32_t effectiveObstacles[9];
   int32_t i, o;
   int32_t canvasIndex, posIndex;


   /* list obstacles */
   posIndex = 0;
   canvasIndex = (demoGameBallState.yPos - 1) * DEMOGAME_FIELD_WIDTH + (demoGameBallState.xPos - 1);

   for (o = 0; o < 3; o++)
   {
      for (i = 0; i < 3; i++)
      {
         actualObstacles[posIndex] = 0;
         effectiveObstacles[posIndex] = 0;

         if (demoGameCanvas[canvasIndex] != DEMOGAME_EMPTY_SPOT_CHAR)
         {
            actualObstacles[posIndex] = 1;
         }

         canvasIndex++;
         posIndex++;
      }

      canvasIndex += DEMOGAME_FIELD_WIDTH - 3;
   }


   /* Check effective obstacles based on the current movement direction */
   effectiveObstacles[1] = actualObstacles[1];
   effectiveObstacles[3] = actualObstacles[3];
   effectiveObstacles[5] = actualObstacles[5];
   effectiveObstacles[7] = actualObstacles[7];

   /* towards the upper left corner */
   if ((demoGameBallState.xVel == -1) && (demoGameBallState.yVel == -1) && (actualObstacles[0] != 0) && (actualObstacles[1] == 0) && (actualObstacles[3] == 0))
   {
      effectiveObstacles[1] = 1;
      effectiveObstacles[3] = 1;
   }

   /* towards the lower left corner */
   if ((demoGameBallState.xVel == -1) && (demoGameBallState.yVel ==  1) && (actualObstacles[6] != 0) && (actualObstacles[3] == 0) && (actualObstacles[7] == 0))
   {
      effectiveObstacles[3] = 1;
      effectiveObstacles[7] = 1;
   }

   /* towards the upper right corner */
   if ((demoGameBallState.xVel ==  1) && (demoGameBallState.yVel == -1) && (actualObstacles[2] != 0) && (actualObstacles[1] == 0) && (actualObstacles[5] == 0))
   {
      effectiveObstacles[1] = 1;
      effectiveObstacles[5] = 1;
   }

   /* towards the lower right corner */
   if ((demoGameBallState.xVel ==  1) && (demoGameBallState.yVel ==  1) && (actualObstacles[8] != 0) && (actualObstacles[5] == 0) && (actualObstacles[7] == 0))
   {
      effectiveObstacles[5] = 1;
      effectiveObstacles[7] = 1;
   }

   /* Check for ball bounces and update direction accordingly */
   if ((demoGameBallState.xVel == -1) && (effectiveObstacles[3] != 0)) demoGameBallState.xVel = -demoGameBallState.xVel;
   if ((demoGameBallState.xVel ==  1) && (effectiveObstacles[5] != 0)) demoGameBallState.xVel = -demoGameBallState.xVel;

   if ((demoGameBallState.yVel == -1) && (effectiveObstacles[1] != 0)) demoGameBallState.yVel = -demoGameBallState.yVel;
   if ((demoGameBallState.yVel ==  1) && (effectiveObstacles[7] != 0)) demoGameBallState.yVel = -demoGameBallState.yVel;
}


void demoGameInternalUpdatePositions()
{
   int32_t i;

   /* move the ball */
   demoGameBallState.speedDivider--;
   if (demoGameBallState.speedDivider < 0)
   {
      demoGameBallState.xPos += demoGameBallState.xVel;
      demoGameBallState.yPos += demoGameBallState.yVel;
      demoGameBallState.speedDivider = DEMOGAME_BALL_SPEED_DIVIDER;
   }

   /* move the ships */
   for (i = 0; i < DEMOGAME_SHIPS_COUNT; i++)
   {
      /* if the ball is moving towards us, or if we are less than half a , move the ship towards the impact point */
      if (((demoGameBallState.yPos < demoGameShipsState[i].yPos) && (demoGameBallState.yVel > 0))
            || ((demoGameBallState.yPos > demoGameShipsState[i].yPos) && (demoGameBallState.yVel < 0)))
      {
         if (demoGameBallState.xPos > demoGameShipsState[i].xPos)
         {
            if (demoGameShipsState[i].xPos < demoGameShipsRestrictions[i].xUpperLimit)
            {
               demoGameShipsState[i].xPos += 1;
            }
         } else {
            if (demoGameBallState.xPos < demoGameShipsState[i].xPos)
            {
               if (demoGameShipsState[i].xPos > demoGameShipsRestrictions[i].xLowerLimit)
               {
                  demoGameShipsState[i].xPos -= 1;
               }
            }
         }
      }

      if ((demoGameBallState.yPos - demoGameShipsState[i].yPos > 2) || (demoGameBallState.yPos - demoGameShipsState[i].yPos < -2))
      {
         demoGameShipsState[i].jumpDivider--;
         if (demoGameShipsState[i].jumpDivider < 0)
         {
            demoGameShipsState[i].jumpDivider = DEMOGAME_JUMP_DIVIDER;

            demoGameShipsState[i].yPos -= demoGameShipsState[i].currentJumpDelta;
            switch (demoGameShipsState[i].currentJumpDelta) {
            case 0 :
               demoGameShipsState[i].currentJumpDelta = 1;
               break;
            case 1 :
               demoGameShipsState[i].currentJumpDelta = 0;
               break;
            }
            demoGameShipsState[i].yPos += demoGameShipsState[i].currentJumpDelta;
         }
      }
   }
}


void demoGameInternalDrawMobiles()
{
   int32_t i;

   demoGameInternalDrawChar(demoGameBallState.xPos, demoGameBallState.yPos, demoGameCanvas, DEMOGAME_BALL_CHAR);

   for (i = 0; i < DEMOGAME_SHIPS_COUNT; i++)
   {
      demoGameInternalDrawSprite(demoGameShipsState[i].xPos, demoGameShipsState[i].yPos, demoGameCanvas, &spriteShip);
   }
}


void demoGameInternalFillTextBox()
{
   static char stringBuffer[DEMOGAME_TEXT_BOX_WIDTH + 1];
   int32_t lineIndex;

   GetResource(DEMOGAMEMUTEX);

   snprintf(stringBuffer, DEMOGAME_TEXT_BOX_WIDTH, " UPTIME %2.2dh %2.2dm %2.2ds  ", (int)demoGameUptimeHours, (int)demoGameUptimeMinutes, (int)demoGameUptimeSeconds);

   demoGameInternalPrintString(
         DEMOGAME_FIELD_WIDTH - DEMOGAME_TEXT_BOX_WIDTH - 2,
         DEMOGAME_FIELD_HEIGHT - DEMOGAME_TEXT_BOX_HEIGHT - 3,
         demoGameCanvas,
         stringBuffer);

   for (lineIndex = 0; lineIndex < DEMOGAME_TEXT_BOX_HEIGHT; lineIndex++)
   {
      demoGameInternalDrawHorLine(
            DEMOGAME_FIELD_WIDTH - DEMOGAME_TEXT_BOX_WIDTH - 2,
            DEMOGAME_FIELD_WIDTH - 3,
            DEMOGAME_FIELD_HEIGHT - DEMOGAME_TEXT_BOX_HEIGHT - 1 + lineIndex,
            demoGameCanvas, DEMOGAME_EMPTY_SPOT_CHAR);

      demoGameInternalPrintString(
            DEMOGAME_FIELD_WIDTH - DEMOGAME_TEXT_BOX_WIDTH - 2,
            DEMOGAME_FIELD_HEIGHT - DEMOGAME_TEXT_BOX_HEIGHT - 1 + lineIndex,
            demoGameCanvas,
            demoGameStringsTable[(demoGameStringsHead + lineIndex) % DEMOGAME_TEXT_BOX_HEIGHT]);
   }

   ReleaseResource(DEMOGAMEMUTEX);
}


void demoGameInternalUpdateScreen()
{
   uint8_t *screenBufferPtr;
   int32_t bufferIndex;

   screenBufferPtr = screenGetActiveScreenBufferPtr();

   for (bufferIndex = 0; bufferIndex < DEMOGAME_FIELD_SIZE; bufferIndex++)
   {
      screenBufferPtr[bufferIndex] = demoGameCanvas[bufferIndex];
   }

   screenUpdateScreen();
}


void demoGameInternalClearTextBox()
{
   int32_t i;

   for (i = 0; i < DEMOGAME_TEXT_BOX_HEIGHT; i++)
   {
      demoGameStringsTable[i][0] = '\0';
   }

   demoGameStringsHead = 0;
   demoGameStringsRow = 0;
   demoGameStringsCol = 0;
}


void demoGameTextBoxEnter()
{
   demoGameStringsRow = (demoGameStringsRow + 1) % DEMOGAME_TEXT_BOX_HEIGHT;

   if (demoGameStringsRow == demoGameStringsHead)
   {
      demoGameStringsHead = (demoGameStringsHead + 1) % DEMOGAME_TEXT_BOX_HEIGHT;
   }

   demoGameStringsCol = 0;
   demoGameStringsTable[demoGameStringsRow][demoGameStringsCol] = '\0';
}

void demoGameTextBoxBackspace()
{
   if (demoGameStringsCol > 0)
   {
      demoGameStringsCol--;
      demoGameStringsTable[demoGameStringsRow][demoGameStringsCol] = '\0';
   }
}

void demoGameInternalInsertKey(key)
{
   if (demoGameStringsCol < DEMOGAME_TEXT_BOX_WIDTH)
   {
      demoGameStringsTable[demoGameStringsRow][demoGameStringsCol] = key;

      demoGameStringsCol++;

      demoGameStringsTable[demoGameStringsRow][demoGameStringsCol] = '\0';
   }
}

/*==================[internal data definition]===============================*/


/*==================[external data definition]===============================*/


/*==================[internal functions definition]==========================*/


/*==================[external functions definition]==========================*/


void demoGameInit()
{
   screenInitModule();

   screenSetBackgroundColor(DEMOGAME_BG_RED, DEMOGAME_BG_GREEN, DEMOGAME_BG_BLUE);
   screenSetForegroundColor(DEMOGAME_FG_RED, DEMOGAME_FG_GREEN, DEMOGAME_FG_BLUE);

   /* Uptime data */
   demoGameUptimeHours = 0;
   demoGameUptimeMinutes = 0;
   demoGameUptimeSeconds = 0;

   /* Bottom ship */
   demoGameShipsState[0].xPos = DEMOGAME_FIELD_WIDTH / 2;
   demoGameShipsState[0].yPos = DEMOGAME_FIELD_HEIGHT - 2;
   demoGameShipsState[0].jumpDivider = DEMOGAME_JUMP_DIVIDER * 3/4;
   demoGameShipsState[0].currentJumpDelta = 1;
   demoGameShipsRestrictions[0].xLowerLimit = spriteShip.width / 2 + 2;
   demoGameShipsRestrictions[0].xUpperLimit = DEMOGAME_FIELD_WIDTH - DEMOGAME_TEXT_BOX_WIDTH - (spriteShip.width - spriteShip.width/2) - 4;

   /* Upper ship */
   demoGameShipsState[1].xPos = DEMOGAME_FIELD_WIDTH / 2;
   demoGameShipsState[1].yPos = 1;
   demoGameShipsState[1].jumpDivider = DEMOGAME_JUMP_DIVIDER * 5/4;
   demoGameShipsState[1].currentJumpDelta = 0;
   demoGameShipsRestrictions[1].xLowerLimit = spriteShip.width / 2 + 2;
   demoGameShipsRestrictions[1].xUpperLimit = DEMOGAME_FIELD_WIDTH - (spriteShip.width - spriteShip.width/2) - 2;

   /* Ball */
   demoGameBallState.xPos = DEMOGAME_FIELD_WIDTH / 2;
   demoGameBallState.yPos = DEMOGAME_FIELD_HEIGHT - 5;
   demoGameBallState.xVel =  1;
   demoGameBallState.yVel = -1;
   demoGameBallState.speedDivider = DEMOGAME_BALL_SPEED_DIVIDER;

   /* Clear the text box */
   demoGameInternalClearTextBox();

   /* Draw the game field */
   demoGameInternalInitFieldGame();
}


void demoGameTickGame()
{
   demoGameInternalCheckColissions();

   demoGameInternalClearMobiles();

   demoGameInternalUpdatePositions();

   demoGameInternalDrawMobiles();

   demoGameInternalFillTextBox();

   demoGameInternalUpdateScreen();
}


void demoGameProcessKey(char key)
{
   GetResource(DEMOGAMEMUTEX);

   switch(key)
   {
   case '\r' :
   case '\n' :
      demoGameTextBoxEnter();
      break;

   case '\b' :
      demoGameTextBoxBackspace();
      break;

   default :
      if ((key >= 32) && (key < 127))
      {
         demoGameInternalInsertKey(key);
      }
      break;
   }

   ReleaseResource(DEMOGAMEMUTEX);
}


void demoGameTimeTick()
{
   GetResource(DEMOGAMEMUTEX);

   demoGameUptimeSeconds++;

   if (demoGameUptimeSeconds >= 60)
   {
      demoGameUptimeSeconds -= 60;

      demoGameUptimeMinutes++;
   }

   if (demoGameUptimeMinutes >= 60)
   {
      demoGameUptimeMinutes -= 60;

      demoGameUptimeHours++;
   }

   ReleaseResource(DEMOGAMEMUTEX);
}


/* **************************** */

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/

