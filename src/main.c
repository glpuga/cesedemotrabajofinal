/* Copyright 2016, Gerardo Puga
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief ciaa_leon3_demo source file
 **
 **
 **/

/** \addtogroup CIAA_Firmware CIAA Firmware
 ** @{ */
/** \addtogroup Examples CIAA Firmware Examples
 ** @{ */
/** \addtogroup ciaa_leon3_demo ciaa_leon3_demo example source file
 ** @{ */

/*
 * Initials     Name
 * ---------------------------
 * GLP          Gerardo Puga
 * */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20161203 v0.0.1   PR   first functional version
 */

/*==================[inclusions]=============================================*/

#include "os.h"
#include "ciaaPOSIX_stdio.h"
#include "ciaaPOSIX_string.h"
#include "ciaak.h"

#include "demogame.h"
#include "uart.h"


/*==================[macros and definitions]=================================*/


#define GAME_UPDATE_PERIOD 3

#define UART_DEVICE_NAME  "/dev/serial/uart/0"

#define STRING_BUFFER_LEN 80


/*==================[internal data declaration]==============================*/


/*==================[internal functions declaration]=========================*/


/*==================[internal data definition]===============================*/


uartHandlerType uartHandler;


/*==================[external data definition]===============================*/


/*==================[internal functions definition]==========================*/


/*==================[external functions definition]==========================*/


int main(void)
{
   StartOS(AppMode1);

   return 0;
}


void ErrorHook(void)
{
   ShutdownOS(0);
}


/* **************************** */

TASK(InitTask)
{
   /* Initialize CIAA kernel and POSIX devices */
   ciaak_start();

   /* Initialize the UART module */
   uartInit(&uartHandler, UART_DEVICE_NAME, ciaaBAUDRATE_9600);

   /* Initialize the demo game module */
   demoGameInit();

   /* Activate the UART receiving task */
   ActivateTask(UartReceivingTask);

   /* Activate the timer task */
   SetRelAlarm(ActivateTimerTask, 100, 100);

   /* Activate the main game task */
   ActivateTask(DemoGameTask);

   /* Terminate the initialization task */
   TerminateTask();
}

/* * **************************** */


TASK(UartReceivingTask)
{
   const char message[] = "Connect at 9600 bps...\n";
   int stringIndex;
   uint8_t dataByte;

   for (stringIndex = 0; message[stringIndex] != 0; stringIndex++)
   {
      demoGameProcessKey(message[stringIndex]);
   }

   while(1)
   {
      uartReadByteBlocking(&uartHandler, &dataByte);

      demoGameProcessKey(dataByte);
   }

}

/* * **************************** */

TASK(DemoGameTask)
{

   demoGameTickGame();

   SetRelAlarm(ActivateDemoGameTask, GAME_UPDATE_PERIOD, 0);

   TerminateTask();
}


/* * **************************** */

TASK(TimerTask)
{
   demoGameTimeTick();

   TerminateTask();
}



/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/

