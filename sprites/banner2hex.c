#include <stdio.h>

void main(void)
{
    int c;
    int rowCounter;

    rowCounter = 0;

    while ((c = getc(stdin)) != EOF)
    {
	if (rowCounter % 32 == 0)
	{
	    printf("\n");
	
	}

	printf(" 0x%2.2x,", (int) c);
	

	rowCounter++;
    }
}

